#!/bin/ash

echo "=== Alpine Linux Exra setup for virtual machine  ==="
apk add virtualbox-guest-additions virtualbox-guest-additions-x11 virtualbox-guest-additions-openrc
rc-update add virtualbox-drm-client default
