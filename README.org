#+title: Alpine Linux Setup

This is a simple Alpine Linux configuration setup with
Xorg, used a a base for various minimal install.


* What I want
For my general purpose base

+ X11
+ Firefox
+ flatpack

* Base Installation

+ Get the ISO
+ Put in on a USB drive
+ Boot on it
+ Login as root by typing
  - ~root~
+ From there, you can type:
  - ~setup-alpine~ 
  - ~us~
  - ~us-colemak~
  - [any hostname]
  - ~eth0~
  - ~dhcp~
  - ~n~
  - [your root psw]
  - ~Asia/Tokyo~
  - ~none~
  - ~busybox~
  - ~f~
  - [your username]
  - [your password]
  - ~openssh~
  - ~sda~
  - ~sys~ or ~crypt~ if you want to cipher the disk
  - ~y~
  - ~reboot~


* Calling the install script

+ Login as ~root~
+ ~apk add nano~
+ ~nano /etc/apk/repositories~ and uncomment all repos
+ ~apk update~
+ ~apk upgrade~
+ ~apk add sudo git~ 
+ ~nano /etc/sudoers~ and give the wheel group access to everything
+ ~adduser~ <your username> ~wheel~
+ ~adduser~ <your username> ~video~
+ ~adduser~ <your username> ~input~
+ ~exit~
+ re-login as you user
+ ~sudo apk update~ just to test sudo
+ ~git clone https://gitlab.com/sunoc/als~
+ If you run Alpine in a VM: ~ash ./als/vm_setup.sh~
+ ~ash ./als/setup.sh $USER~
  
