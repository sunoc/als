#!/bin/ash

# ==============================================================================
echo ">>>>>> Alpine Linux Setup"

# ==============================================================================
echo ">>>>>> Xorg setup"
setup-xorg-base xf86-video-intel xf86-input-evdev setxkbmap


# ==============================================================================
echo ">>>>>> Installing fonts"
apk add terminus-font ttf-inconsolata ttf-dejavu font-noto font-noto-cjk \
    ttf-font-awesome font-noto-extra

apk add terminus-font font-noto font-noto-thai font-noto-tibetan font-ipa \
    font-sony-misc font-daewoo-misc font-jis-misc

fc-cache -f

# ==============================================================================
echo ">>>>>> Setup sound"
adduser $1 audio
apk add alsa-utils alsa-utils-doc alsa-lib alsaconf
rc-service alsa start
rc-update add alsa

# ==============================================================================
echo ">>>>>> Installing firefox"
apk add firefox adwaita-icon-theme


# ==============================================================================
echo ">>>>>> Install flatpaks base"
apk add flatpak
adduser $1 flatpak
flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo


# ==============================================================================
echo ">>>>>> Prepare the init scripts"
cp /home/$1/als/.profile /home/$1/
cp /home/$1/als/.xinitrc /home/$1/

echo ">>>>>> First start of Xorg setup"
reboot
